## Bot Labs

Project focused on learning, developing and deploying web spiders for data collection.

---

## Overview

Web spiders are developed using Python and Scrapy framework.

---

## User requirements

Web spiders, latter refered to as spiders, are programs running in the background of the server.
They are fed with set of links to be scanned. There is possibility to create dedicated spiders for certain purposes. Nevertheless, the output of every of them must be conservative JSON file, suitable for further processing.

---

## Convention

Folder "examples" is aimed to be a playground for developers, to test and tinker with new solutions, technologies.
The contents of "examples" can be modified and removed any time necessary.
Folder "project" contains "beta" and "release" folders, each of them respectively the proper content.

---

## License

Code is distributed under MIT License. You should obtain license with code.

---

## Feedback 

Feel free to provide feedback on the code at krzste09@gmail.com.