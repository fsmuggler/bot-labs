class Address:
    def __init__(self, country = None, state = None, city = None, street = None,houseNo = None, roomNo = None):
        self.country = country or "defaultCountry"
        self.state = state or "defaultState"
        self.city = city or "defaultCity"
        self.street = street or "defaultStreet"
        self.houseNo = houseNo
        self.roomNo = roomNo
    
    def getCountry(self):
        return self.country

    def getState(self):
        return self.state

    def getCity(self):
        return self.city

    def getStreet(self):
        return self.street
    
    def getHouseNo(self):
        return self.houseNo

    def getRoomNo(self):
        return self.roomNo
    
    